use std::{env, time::Instant};

fn gaussian_sum(n: &u128) -> u128 {
    let mut result: u128 = 0;
    for i in 0..*n {
        result += i;
    }
    return result;
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let n: u128 = args[1].parse().expect("Invalid n!");
    let time_nano: u128;
    let time_micro: u128;
    let time_millis: u128;

    let now = Instant::now();
    let sum = gaussian_sum(&n);
    time_nano = now.elapsed().as_nanos();
    time_micro = now.elapsed().as_micros();
    time_millis = now.elapsed().as_millis();
    
    println!("Result for {} is : {}\nTook: {}ns / {}mc / {}ms", &n, &sum, &time_nano, &time_micro, &time_millis);
}